#coding:utf-8

import spacy
import urllib
import pandas as pd
import sqlalchemy
import re
import datetime
import sys
from collections import defaultdict

#Cria a conexÃ£o e o cursor
connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=sql-convex.database.windows.net;DATABASE=mrv;UID=convex;PWD=SF0xvcUPGrt81'
connection_string = urllib.parse.quote_plus(connection_string)
connection_string = 'mssql+pyodbc:///?odbc_connect=%s' % connection_string
engine = sqlalchemy.create_engine(connection_string)
e = engine.connect()

#Carrega o Dataframe com o teor jÃ¡ pÅe tratado na query
data = pd.read_sql("select mv.idprocesso, idmovimento, dtmovimento,  SUBSTRING(deteormovimento, (len(deteormovimento) - CHARINDEX(REVERSE('nomeio'), REVERSE(deteormovimento)) - 4), 100) as deteormovimento from pg_movimentos mv  join HIERARQUIAMOVIMENTOS hm on hm.deTipoMovimento = mv.detipomovimento and mv.detribunal = hm.deTribunal  where mv.deteormovimento like '%nomeio%Perito%' and cdSituacao =   'V'",e)

#Carrega o modelo
nlp = spacy.load("./modelo")

#FunÃ§Ã£o que executa a extraÃ§Ã£o da entidade
def extper(deteormovimento):
    texto = deteormovimento.replace("\n", "").replace('"','')
    nlu = nlp(texto)
    peritos = list()
    for ent in nlu.ents:
        perito = (ent.text.upper())
        perito = re.sub('-.*','',perito)
        return perito

#Dispara a ExecuÃ§Ã£o da funÃ§Ã£o para extraÃ§Ã£o de perito em cada linha do DataFrame
data['Perito'] = data.apply(lambda row: extper(row.deteormovimento), axis=1)

#Selecion apenas as colunas que devem ser inseridas na tabela PG_EXTRACAOPERITO
data = data[['idprocesso','idmovimento','dtmovimento','Perito']]

rows = str(data.count()['idmovimento']) #Conta a quantidade de linhas inseridas para o LOG de evento


getdate = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S') #Carrega a data atual para o LOG de evento

#Insere os dados no banco
data.to_sql('PG_EXTRACAOPERITO', e, if_exists='replace')

print('EXTRAÃÃO DE PERITOS - ROTINA DE EXTRAÃÃO FINALIZADA!')


#Insere o LOG da execuÃ§Ã£o - O codigo foi alterado devido ao erro apresentado no DRIVE ODBC 13
insert = 'INSERT INTO LOG_FUNC(DTPROCESSAMENTO, TPEVENTO, QNTLINHAS) VALUES'
values = (getdate, 'PG_EXTRACAOPERITO', rows)
values = str(values)
sql = (insert+values)
sql = str(sql)
e.execute(sql)
print('EXTRAÃÃO DE PERITOS - LOG INSERIDO!')